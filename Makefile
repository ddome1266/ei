OWLURL=https://gitlab.com/owl-lisp/owl/uploads/92375620fb4d570ee997bc47e2f6ddb7/ol-0.1.21.c.gz

everything: ei
	test/run ./ei

ei: ei.c
	cc -O2 -o ei ei.c

ei.c: ei.scm
	make ol
	./ol -o ei.c ei.scm

ol:
	test -f ol.c || curl $(OWLURL) | gzip -d > ol.c
	cc -O2 -o ol ol.c

install: ei
	install -m 755 ei /usr/bin/ei

uninstall:
	-rm /usr/bin/ei
	
clean:
	-rm ei.c ei
	-rm ol

tarball: ei
	mkdir ei-${VERSION}
	cp -va ei.c ei.scm test Makefile ei-${VERSION}
	tar -f - -c ei-${VERSION} | gzip -9 > ei-${VERSION}.tar.gz

mrproper: clean
	-rm ol.c
